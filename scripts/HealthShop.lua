local ai = require "necro.game.enemy.ai.AI"
local components = require "necro.game.data.Components"
local field = components.field
local event = require "necro.event.Event"
local customEntities = require "necro.game.data.CustomEntities"
local minimapTheme = require "necro.game.data.tile.MinimapTheme"
local attack = require "necro.game.character.Attack"
local team = require "necro.game.character.Team"
local object = require "necro.game.object.Object"
local priceTag = require "necro.game.item.PriceTag"
local move = require "necro.game.system.Move"
local spell = require "necro.game.spell.Spell"
local ecs = require "system.game.Entities"

local hcss, customSecretShop = pcall(require, "CustomSecretShop.CustomSecretShop")

if not hcss then
    error("Need to have CustomSecretShop mod active to use Health Shop")
end

components.register {
    magicHealthTile = {
        field.entityID("shopkeeper"),
        field.int16("targetX"),
        field.int16("targetY"),
    },
    healthShopkeeper = {}
}

customEntities.extend {
	name = "Medic",
	template = customEntities.template.enemy("transmogifier"),
    components = {
        initialEquipment = {items = {"CursedPotion"}},
        priceTagCostCurrency = {cost = 4},
        priceTag = {},
        priceTagUnaffordableFlyaway = {text = "Need %d coins"},
        priceTagMultiplyCostOnPurchase = {},
        priceTagDepthMultiplier = {},
        priceTagLabel = {offsetX = 0, offsetY =-25},
        soundPriceTagPurchaseSuccess = {playWhenFree = false},
        soundPriceTagPurchaseFail = {},
        sprite = {
            texture = "mods/HealthShop/gfx/medic.png"
        },
        positionalSprite = {},
        shopkeeper = {},
        homeArea = {},
        minimapStaticPixel = {color = minimapTheme.Color.NPC},
        inventory = {},
        inventoryDropItemsOnDeath = {},
        inventoryMarkSeen = {},
        HealthShop_healthShopkeeper = {},
        provokable = {},
        secretShopLabel = {text = "Purchase some health?", offsetY = 15},
        normalAnimation = {frames = {1,1,2,2}},
        attackable = {flags = attack.Flag.PROVOKE}
    }
}

customEntities.register {
    name = "MagicHealthTile",
    gameObject = {},
    trap = {
        triggerDelay = 0,
        targetFlags = attack.Flag.DEFAULT,
    },
    trapArmOnVictimMove = {},
    team = {id = team.Id.ENEMY},
    position = {},
    sprite = {texture = "mods/HealthShop/gfx/magicTileMedic.png"},
    positionalSprite = { offsetY = 12 },
	spriteDecal = { z = 1 },
	darkenToTileBrightnessEffect = {},
	despawnOnTileChange = {},
	sale = {},
    HealthShop_magicHealthTile = {},
    visibility = {},
}

--local shopID =  customSecretShop.addCustomShop("HealthShop", "HealthShop", 5, 7, 
--"mods/HealthShop/gfx/healthShopTravelRune.png", customSecretShop.isAnyNonBossFloor,  
--customSecretShop.normalCrackedWall(1), 1, {TimeShop_canDeactivate = {}})
--local shopID = customSecretShop.addDemoRoom("HealthShop", "HealthShop", {}, "mods/HealthShop/gfx/healthShopTravelRune.png")
local shopID = customSecretShop.addSecretShop("HealthShop", "HealthShop", 5, 7, "mods/HealthShop/gfx/healthShopTravelRune.png", 2, {})

event.levelLoad.add("fillHealthShop", {order = "entities", sequence = 2}, function(ev)
    local bounds = customSecretShop.getBounds(shopID)
    if bounds then
        local cx = bounds[1] + math.floor(bounds[3] * 1/2)
        local cy = bounds[2] + math.floor(bounds[4] * 1/2)
        object.spawn("HealthShop_Medic", cx, cy - 2)
    end
end)
event.objectSpawn.add("makeHealthShop", {order = "spawnExtras", filter = {"HealthShop_healthShopkeeper"}, sequence = 2}, function(ev)
    local x = ev.x
	local y = ev.y + 2

    local comps = {
       magicTile = { targetX = x, targetY = y + 3 },
       sale = {priceTag = ev.entity.id}
    }
    object.spawn("HealthShop_MagicHealthTile", x, y, comps)
end)

event.trapTrigger.add("processMagicHealthTile", {order = "magicTile", filter = "HealthShop_magicHealthTile", sequence = 1}, function(ev)
    if ev.victim:hasComponent("inventory") and ev.victim:hasComponent("previousPosition") then
        if ecs.getEntityByID(ev.trap.sale.priceTag) then
            if priceTag.pay(ev.victim, ev.trap) then
                spell.cast(ev.victim,"SpellcastHealPixie")
            end
                move.absolute(ev.victim, ev.victim.previousPosition.x, ev.victim.previousPosition.y)
        end
    end
end)