Dependency: Requires CustomSecretShop (1.1)

This adds a health shop, a new type of secret shop. It contains a single tile with a similar style to the conjur/transmog that allows you to purchase healing. The cost on 1-1 is 10 gold, and it doubles every time you use it.
The shop also includes the shopkeepeer: the medic. A healer at heart, he cannot defend himself and dies to any attack, dropping a cursed potion. It was a normal potion, but got cursed by your own sins. You monster.

All of the sprites were by Sipher Nil, who also came up with the original mod idea.